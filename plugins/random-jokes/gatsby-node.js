const axios = require('axios');
const crypto = require('crypto');

const API_URI =
    'https://pokeapi.co/api/v2/pokemon/';

exports.sourceNodes = async ({ boundActionCreators, createContentDigest }) => {
    const { createNode } = boundActionCreators;
    const result = await axios.get(API_URI);

    //console.log(result.data.results);

    let i = 0;
    for (const pokemon of result.data.results) {
        const id = pokemon.url.split('/')[6];
        const additionalPokemonData = await axios.get(`${API_URI}${id}`)


        await createNode({
            name: pokemon.name,
            parent: null,
            id,
            abilities: additionalPokemonData.data.abilities,
            stats: additionalPokemonData.data.stats,
            types: additionalPokemonData.data.types,
            children: [],
            internal: {
                type: 'Pokemon',
                contentDigest: createContentDigest(pokemon),
            },
        });
    }
};
