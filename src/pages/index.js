import React from "react"
import { StaticQuery, graphql } from "gatsby"

import Layout from "../components/layout"
import styles from "./index.modules.scss"

const IndexPage = () => (
  <Layout>
    <StaticQuery
      query={graphql`
      query{
        allPokemon {
          edges {
            node {
              name
              types{
                type {
                  name
                }
              }
            }
          }
        }
      }
    `}
      render={data => (
        <div>
          <ul>{getPokemonInfo(data)}</ul>
        </div>
      )}
    />

  </Layout>
)

const getPokemonInfo = data => {
  const pokemonArray = [];
  data.allPokemon.edges.forEach(item => {
    pokemonArray.push(<li className={getPokemonClassNamesFromTypes(item.node.types)} key={item.node.name}><p>{item.node.name} - {getPokemonClassNamesFromTypes(item.node.types)}</p></li>)
  });
  return pokemonArray;
}

const getPokemonClassNamesFromTypes = (types) => {
  const allTypes = [];
  types.forEach((type) => allTypes.push(type.type.name))
  return allTypes.join(' ');

}

export default IndexPage;
